const router = require('express').Router();
const TodoItemsModel = require('../models/TodoItems');
const { postTodoItem, getTodoItems, updateSelectedItem, deleteSelectedItem } = require('../controller/todoItems');

//post or add item into the database
router.post('/api/todos', postTodoItem);

//get items from database
router.get('/api/todos', getTodoItems);

//update selected item. soft delete | update item | complete or incomplete
router.put('/api/todos/:id', updateSelectedItem);

//hard delete selected item
router.delete('/api/todos/:id', deleteSelectedItem);

module.exports = router;