const mongoose = require('mongoose');

//Todo Schema
const TodoItemsSchema = new mongoose.Schema({
  item: {
    type: String,
    required: true,
  },
  is_completed: {
    type: Boolean,
    default: false,
  },
  date_created: {
    type: Date,
    default: Date.now
  },
  date_updated: { type: Date },
  date_deleted: { type: Date },
});

module.exports = mongoose.model('todos', TodoItemsSchema);