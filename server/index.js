const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const app = express();

//get data in json format
app.use(express.json());

//use cors to allows access from different address
app.use(cors());

//routes
const TodoItemRoute = require('./routes/todoItems');
app.use('/', TodoItemRoute);

//connect to database
mongoose.connect(process.env.DATABASE_CONNECTION, {
  useUnifiedTopology: true,
  useNewUrlParser: true,
})
  .then(() => console.log('Connected to Database'))
  .catch(err => console.log('Error connecting to MongoDB:', err));

//add port and listen
const port = process.env.PORT || 5500;
app.listen(port, () => console.log('Connected to Server'));

module.exports = app;