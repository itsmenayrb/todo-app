const TodoItemsModel = require('../models/TodoItems');

//post or add item into the database
postTodoItem = async (req, res) => {
  try {
    const newTodoItem = new TodoItemsModel({
      item: req.body.item,
    });

    const saveTodoItem = await newTodoItem.save();
    res.status(200).json(saveTodoItem);
  } catch (err) {
    res.json(err);
  }
};

//get items from database
getTodoItems = async (req, res) => {
  try {
    const limit = parseInt(req.query.limit || "10");
    const page = parseInt(req.query.page || "0");
    const total = await TodoItemsModel.countDocuments({
      date_deleted: null,
    });
    const todoItems = await TodoItemsModel.find({
      date_deleted: null,
    })
      .limit(limit)
      .skip(limit * page);
    res.status(200).json({totalPages: Math.ceil(total / limit), todoItems});
  } catch (err) {
    res.json(err);
  }
};

//update selected item
updateSelectedItem = async (req, res) => {
  try {
    const updateTodoItem = await TodoItemsModel.findByIdAndUpdate(req.params.id, {$set: req.body});
    const updatedItem = {
      ...updateTodoItem._doc,
      item: req.body.item || updateTodoItem.item,
      is_completed: req.body.is_completed || updateTodoItem.is_completed,
      date_updated: req.body.date_updated || new Date().toISOString(),
    };
    res.status(200).json(updatedItem);
  } catch (err) {
    res.json(err);
  }
};

//hard delete selected item
deleteSelectedItem = async (req, res) => {
  try {
    await TodoItemsModel.findByIdAndDelete(req.params.id);
    res.status(200).json({ message: 'Item Deleted' });
  } catch (err) {
    res.json(err);
  }
};

module.exports = {
  postTodoItem,
  getTodoItems,
  updateSelectedItem,
  deleteSelectedItem
}