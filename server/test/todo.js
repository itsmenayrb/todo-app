const TodoItemsModel = require('../models/TodoItems');
const mongoose = require('mongoose');

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../index');
const should = chai.should();
const expect = chai.expect;

chai.use(chaiHttp);

describe('todos', () => {
  /** Testing GET route */
  describe('/GET todos', () => {
    it('it should GET all the todo items', (done) => {
      chai.request(server)
        .get('/api/todos')
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('totalPages');
          res.body.should.have.property('todoItems');
        done();
        });
    });
  });

  /** Testing POST route */
  describe('/POST todos', () => {
    it('it should not post an ITEM without item field', (done) => {
      const payload = {
        test: "hooray",
      };
      chai.request(server)
        .post('/api/todos')
        .send(payload)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('errors');
          res.body.errors.should.have.property('item');
          res.body.errors.item.should.have.property('kind').eql('required');
        done();
        });
    });
  });

  describe('/POST todos', () => {
    it('it should post an ITEM', (done) => {
      const payload = {
        item: "hooray",
      };
      chai.request(server)
        .post('/api/todos')
        .send(payload)
        .set('Content-Type', 'application/json')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.property('item');
          res.body.should.have.property('_id');
          res.body.should.have.property('date_created');
        done();
        });
    });
  });

  /** Testing PUT route */
  describe('/PUT todos', () => {
    it('it should UPDATE an ITEM given the id', (done) => {
      const item = new TodoItemsModel({ item: 'New Todo List' });
      item.save((error, itm) => {
        chai.request(server)
          .put('/api/todos/' + itm._id)
          .send({ item: "New Todo List Updated", date_updated: Date.now() })
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('item');
            res.body.should.have.property('_id');
          done();
          });
      })
    });

    it('it should SOFT DELETE an ITEM given the id', (done) => {
      const item = new TodoItemsModel({ item: 'New Todo List' });
      item.save((error, itm) => {
        chai.request(server)
          .put('/api/todos/' + itm._id)
          .send({ item: itm.item, date_deleted: Date.now() })
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('item');
            res.body.should.have.property('_id');
          done();
          });
      })
    });

    it('it should toggle an ITEM to COMPLETE/INCOMPLETE given the id', (done) => {
      const item = new TodoItemsModel({ item: 'New Todo List' });
      item.save((error, itm) => {
        chai.request(server)
          .put('/api/todos/' + itm._id)
          .send({ item: itm.item, date_updated: Date.now(), is_completed: !itm.is_completed })
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('item');
            res.body.should.have.property('_id');
          done();
          });
      })
    });
  });

  /** Testing PUT route */
  describe('/DELETE todos', () => {
    it('it should HARD DELETE an ITEM given the id', (done) => {
      const item = new TodoItemsModel({ item: 'New Todo List' });
      item.save((error, itm) => {
        chai.request(server)
          .delete('/api/todos/' + itm._id)
          .set('Content-Type', 'application/json')
          .end((err, res) => {
            res.should.have.status(200);
            res.body.should.be.a('object');
            res.body.should.have.property('message').eql('Item Deleted');
          done();
          });
      })
    });
  });
});
